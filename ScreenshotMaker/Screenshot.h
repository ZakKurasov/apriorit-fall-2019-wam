#pragma once

#include <Windows.h>
#include <iostream>
#include <vector>

class Screenshot
{

public:

	std::vector<char> screenshot;


	Screenshot();
	
	~Screenshot();

	
	void TakeScreenshot();

	char* GetScreenshotBuffer();

	uint32_t GetScreenshotSize();

	int GetScreenshotWidth();

	int GetScreenshotHeight();

private:

	HBITMAP hBitmap;
	HDC hScreenDC;
	HDC hMemoryDC;
	int screenshot_Width;
	int screenshot_Height;
	

};


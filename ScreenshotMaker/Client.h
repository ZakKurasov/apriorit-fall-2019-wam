#pragma once

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include "Screenshot.h"
#include <string>

using boost::asio::ip::tcp;


class Client
{
        enum class WAMStatus : char {
            WAMStart = 's',
            WAMStop = 'e'
        };
public:

	Client(const char* host);
	
	~Client();

        void Connect();

        void SendHearthbeat();
        void SendScreenshot();

        WAMStatus ReceiveAnswer();

	void Run(WAMStatus status);

	void Start();

	std::string ReadAddressFromRegistry();

	boost::asio::ssl::context CreateSSL_Context();

private:
	boost::asio::io_context io_context;
	boost::asio::ssl::context context;
	boost::asio::ssl::stream<tcp::socket> s;
	tcp::resolver resolver;
	std::string serverAddress;
	volatile bool connected;
	volatile bool running;
	Screenshot screenshot;
	const int delay=1000;
	const int port=8000;
	char answer[2] = { 0 };
};


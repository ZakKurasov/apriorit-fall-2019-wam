#include "Client.h"
#include <iostream>


int main(int argc, char** args)
{
	::ShowWindow(::GetConsoleWindow(), SW_SHOW);
        if (argc < 2)
        {
            std::cout << "you should pass ip as second argument" << std::endl;
            return 1;
        }
        try
	{
		Client client(args[1]);
		client.Start();
	}
	catch (std::exception & e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}
	system("pause");
	return 0;
}
#include "Screenshot.h"

Screenshot::Screenshot()
	:screenshot_Height(0), screenshot_Width(0)
{
	// get the device context of the screen
	hScreenDC = CreateDC("DISPLAY", nullptr, nullptr, nullptr);
	// and a device context to put it in
	hMemoryDC = CreateCompatibleDC(hScreenDC);
	screenshot_Width = GetDeviceCaps(hScreenDC, HORZRES);
	screenshot_Height = GetDeviceCaps(hScreenDC, VERTRES);
	screenshot.resize(GetScreenshotSize());
}

Screenshot::~Screenshot()
{
	DeleteDC(hMemoryDC);
	DeleteDC(hScreenDC);
}

void Screenshot::TakeScreenshot()
{
	screenshot.clear();
	// get the device context of the screen
	hScreenDC = CreateDC("DISPLAY", nullptr, nullptr, nullptr);
	// and a device context to put it in
	hMemoryDC = CreateCompatibleDC(hScreenDC);
	screenshot_Width = GetDeviceCaps(hScreenDC, HORZRES);
	screenshot_Height = GetDeviceCaps(hScreenDC, VERTRES);
	//screenshot.resize(GetScreenshotSize());
	hBitmap = CreateCompatibleBitmap(hScreenDC, screenshot_Width, screenshot_Height);
	HBITMAP hOldBitmap = static_cast<HBITMAP>(SelectObject(hMemoryDC, hBitmap));

	BitBlt(hMemoryDC, 0, 0, screenshot_Width, screenshot_Height, hScreenDC, 0, 0, SRCCOPY);
	hBitmap = static_cast<HBITMAP>(SelectObject(hMemoryDC, hOldBitmap));

	GetBitmapBits(hBitmap, screenshot_Width * screenshot_Height * 4, screenshot.data());
}

char* Screenshot::GetScreenshotBuffer()
{
        return screenshot.data();
}

uint32_t Screenshot::GetScreenshotSize()
{
	return screenshot_Height * screenshot_Width * 4;
}

int Screenshot::GetScreenshotWidth()
{
	return screenshot_Width;
}

int Screenshot::GetScreenshotHeight()
{
	return screenshot_Height;
}
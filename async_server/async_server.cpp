#include <cstdlib>
#include <iostream>
#include <memory>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/bind.hpp>
using boost::asio::ip::tcp;

typedef boost::asio::ssl::stream<tcp::socket> ssl_socket;

LRESULT CALLBACK PictViewWndProc(HWND, UINT, UINT, LONG);

enum class WAMStatus : char {
    WAMStart = 's',
    WAMStop = 'e'
};

enum class MsgType : char
{
    HearthBeat = 'h',
    Screenshot = 's'
};

const std::string cert = "-----BEGIN CERTIFICATE-----\n"
"MIIDaDCCAlCgAwIBAgIJAO8vBu8i8exWMA0GCSqGSIb3DQEBCwUAMEkxCzAJBgNV\n"
"BAYTAlVTMQswCQYDVQQIDAJDQTEtMCsGA1UEBwwkTG9zIEFuZ2VsZXNPPUJlYXN0\n"
"Q049d3d3LmV4YW1wbGUuY29tMB4XDTE3MDUwMzE4MzkxMloXDTQ0MDkxODE4Mzkx\n"
"MlowSTELMAkGA1UEBhMCVVMxCzAJBgNVBAgMAkNBMS0wKwYDVQQHDCRMb3MgQW5n\n"
"ZWxlc089QmVhc3RDTj13d3cuZXhhbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUA\n"
"A4IBDwAwggEKAoIBAQDJ7BRKFO8fqmsEXw8v9YOVXyrQVsVbjSSGEs4Vzs4cJgcF\n"
"xqGitbnLIrOgiJpRAPLy5MNcAXE1strVGfdEf7xMYSZ/4wOrxUyVw/Ltgsft8m7b\n"
"Fu8TsCzO6XrxpnVtWk506YZ7ToTa5UjHfBi2+pWTxbpN12UhiZNUcrRsqTFW+6fO\n"
"9d7xm5wlaZG8cMdg0cO1bhkz45JSl3wWKIES7t3EfKePZbNlQ5hPy7Pd5JTmdGBp\n"
"yY8anC8u4LPbmgW0/U31PH0rRVfGcBbZsAoQw5Tc5dnb6N2GEIbq3ehSfdDHGnrv\n"
"enu2tOK9Qx6GEzXh3sekZkxcgh+NlIxCNxu//Dk9AgMBAAGjUzBRMB0GA1UdDgQW\n"
"BBTZh0N9Ne1OD7GBGJYz4PNESHuXezAfBgNVHSMEGDAWgBTZh0N9Ne1OD7GBGJYz\n"
"4PNESHuXezAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQCmTJVT\n"
"LH5Cru1vXtzb3N9dyolcVH82xFVwPewArchgq+CEkajOU9bnzCqvhM4CryBb4cUs\n"
"gqXWp85hAh55uBOqXb2yyESEleMCJEiVTwm/m26FdONvEGptsiCmF5Gxi0YRtn8N\n"
"V+KhrQaAyLrLdPYI7TrwAOisq2I1cD0mt+xgwuv/654Rl3IhOMx+fKWKJ9qLAiaE\n"
"fQyshjlPP9mYVxWOxqctUdQ8UnsUKKGEUcVrA08i1OAnVKlPFjKBvk+r7jpsTPcr\n"
"9pWXTO9JrYMML7d+XRSZA1n3856OqZDX4403+9FnXCvfcLZLLKTBvwwFgEFGpzjK\n"
"UEVbkhd5qstF6qWK\n"
"-----END CERTIFICATE-----\n";

const std::string kPrivateKey = "-----BEGIN PRIVATE KEY-----\n"
"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDJ7BRKFO8fqmsE\n"
"Xw8v9YOVXyrQVsVbjSSGEs4Vzs4cJgcFxqGitbnLIrOgiJpRAPLy5MNcAXE1strV\n"
"GfdEf7xMYSZ/4wOrxUyVw/Ltgsft8m7bFu8TsCzO6XrxpnVtWk506YZ7ToTa5UjH\n"
"fBi2+pWTxbpN12UhiZNUcrRsqTFW+6fO9d7xm5wlaZG8cMdg0cO1bhkz45JSl3wW\n"
"KIES7t3EfKePZbNlQ5hPy7Pd5JTmdGBpyY8anC8u4LPbmgW0/U31PH0rRVfGcBbZ\n"
"sAoQw5Tc5dnb6N2GEIbq3ehSfdDHGnrvenu2tOK9Qx6GEzXh3sekZkxcgh+NlIxC\n"
"Nxu//Dk9AgMBAAECggEBAK1gV8uETg4SdfE67f9v/5uyK0DYQH1ro4C7hNiUycTB\n"
"oiYDd6YOA4m4MiQVJuuGtRR5+IR3eI1zFRMFSJs4UqYChNwqQGys7CVsKpplQOW+\n"
"1BCqkH2HN/Ix5662Dv3mHJemLCKUON77IJKoq0/xuZ04mc9csykox6grFWB3pjXY\n"
"OEn9U8pt5KNldWfpfAZ7xu9WfyvthGXlhfwKEetOuHfAQv7FF6s25UIEU6Hmnwp9\n"
"VmYp2twfMGdztz/gfFjKOGxf92RG+FMSkyAPq/vhyB7oQWxa+vdBn6BSdsfn27Qs\n"
"bTvXrGe4FYcbuw4WkAKTljZX7TUegkXiwFoSps0jegECgYEA7o5AcRTZVUmmSs8W\n"
"PUHn89UEuDAMFVk7grG1bg8exLQSpugCykcqXt1WNrqB7x6nB+dbVANWNhSmhgCg\n"
"VrV941vbx8ketqZ9YInSbGPWIU/tss3r8Yx2Ct3mQpvpGC6iGHzEc/NHJP8Efvh/\n"
"CcUWmLjLGJYYeP5oNu5cncC3fXUCgYEA2LANATm0A6sFVGe3sSLO9un1brA4zlZE\n"
"Hjd3KOZnMPt73B426qUOcw5B2wIS8GJsUES0P94pKg83oyzmoUV9vJpJLjHA4qmL\n"
"CDAd6CjAmE5ea4dFdZwDDS8F9FntJMdPQJA9vq+JaeS+k7ds3+7oiNe+RUIHR1Sz\n"
"VEAKh3Xw66kCgYB7KO/2Mchesu5qku2tZJhHF4QfP5cNcos511uO3bmJ3ln+16uR\n"
"GRqz7Vu0V6f7dvzPJM/O2QYqV5D9f9dHzN2YgvU9+QSlUeFK9PyxPv3vJt/WP1//\n"
"zf+nbpaRbwLxnCnNsKSQJFpnrE166/pSZfFbmZQpNlyeIuJU8czZGQTifQKBgHXe\n"
"/pQGEZhVNab+bHwdFTxXdDzr+1qyrodJYLaM7uFES9InVXQ6qSuJO+WosSi2QXlA\n"
"hlSfwwCwGnHXAPYFWSp5Owm34tbpp0mi8wHQ+UNgjhgsE2qwnTBUvgZ3zHpPORtD\n"
"23KZBkTmO40bIEyIJ1IZGdWO32q79nkEBTY+v/lRAoGBAI1rbouFYPBrTYQ9kcjt\n"
"1yfu4JF5MvO9JrHQ9tOwkqDmNCWx9xWXbgydsn/eFtuUMULWsG3lNjfst/Esb8ch\n"
"k5cZd6pdJZa4/vhEwrYYSuEjMCnRb0lUsm7TsHxQrUd6Fi/mUuFU/haC0o0chLq7\n"
"pVOUFq5mW8p0zbtfHbjkgxyF\n"
"-----END PRIVATE KEY-----\n";

const std::string dh =
"-----BEGIN DH PARAMETERS-----\n"
"MIIBCAKCAQEArzQc5mpm0Fs8yahDeySj31JZlwEphUdZ9StM2D8+Fo7TMduGtSi+\n"
"/HRWVwHcTFAgrxVdm+dl474mOUqqaz4MpzIb6+6OVfWHbQJmXPepZKyu4LgUPvY/\n"
"4q3/iDMjIS0fLOu/bLuObwU5ccZmDgfhmz1GanRlTQOiYRty3FiOATWZBRh6uv4u\n"
"tff4A9Bm3V9tLx9S6djq31w31Gl7OQhryodW28kc16t9TvO1BzcV3HjRPwpe701X\n"
"oEEZdnZWANkkpR/m/pfgdmGPU66S2sXMHgsliViQWpDCYeehrvFRHEdR9NV+XJfC\n"
"QMUk26jPTIVTLfXmmwU0u8vUkpR7LQKkwwIBAg==\n"
"-----END DH PARAMETERS-----\n";

class session
	: public std::enable_shared_from_this<session>
{
public:
	session(const HWND& hw, boost::asio::io_context& io_context, boost::asio::ssl::context& context)
		: socket_(io_context, context), fl(WAMStatus::WAMStop), hWnd(hw)
	{
	}

	void start()
	{
                active = true;
		socket_.async_handshake(
                    boost::asio::ssl::stream_base::server,
		    boost::bind(&session::handle_handshake, this, boost::asio::placeholders::error)
                );
	}

        void enable()
	{
                fl = WAMStatus::WAMStart;
                InvalidateRect(hWnd, 0, true);
	}

        void disable()
	{
                fl = WAMStatus::WAMStop;
                InvalidateRect(hWnd, 0, true);
	}

        void deactivate()
	{
                active = false;
                disable();
	}

	ssl_socket::lowest_layer_type& socket()
	{
		return socket_.lowest_layer();
	}
	void handle_handshake(const boost::system::error_code& error)
	{
		if (!error)
		{
                    write_s();
		}
                else
                {
                    deactivate();
                }
	}

        void write_s()
	{
                socket_.async_write_some(
                    boost::asio::buffer(&fl, sizeof(WAMStatus)),
                    boost::bind(&session::read_mtype, this, boost::asio::placeholders::error)
                );
	}

        void read_mtype(const boost::system::error_code& error)
	{
	        if (!error)
	        {
                    socket_.async_read_some(
                        boost::asio::buffer(&mtype, sizeof(MsgType)),
                        boost::bind(&session::handle_mtype, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
                    );
	        }
                else
                {
                    deactivate();
                }
	}

        void handle_mtype(const boost::system::error_code& error, size_t n)
	{
	        if (!error)
	        {
	                if (mtype == MsgType::HearthBeat)
	                {
                            Sleep(1000);
                            write_s();
	                }
                        else
                        {
                            read_w();
                        }
	        }
                else
                {
                    deactivate();
                }
	}

        void read_w()
        {
                socket_.async_read_some(
                    boost::asio::buffer(&Size_.width, sizeof(Size_.width)),
                    boost::bind(&session::read_h, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
                );
        }

	void read_h(const boost::system::error_code& error, size_t bytes_transferred)
	{
		if (!error)
		{
			socket_.async_read_some(
                            boost::asio::buffer(&Size_.height, sizeof(Size_.height)),
			    boost::bind(&session::read_p, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
                        );
		}
                else
                {
                    deactivate();
                }
	}

        void read_p(const boost::system::error_code& error, size_t bytes_transferred)
        {
            if (!error)
            {
                socket_.async_read_some(
                    boost::asio::buffer(data, 60000),
                    boost::bind(&session::write_o, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
                );
            }
            else
            {
                deactivate();
            }
        }

	void write_o(const boost::system::error_code& error, size_t bytes_transferred)
	{
		if (!error)
		{
			data_all.insert(data_all.end(), data.begin(), data.begin() + bytes_transferred);
			if (data_all.size() < Size_.width * Size_.height * 4)
			{
                            socket_.async_read_some(
                                boost::asio::buffer(data, 60000),
                                boost::bind(&session::write_o, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
                            );
                            return;
                        }

                        HDC hScreenDC = CreateDC(L"DISPLAY", nullptr, nullptr, nullptr);
                        HDC hdc = CreateCompatibleDC(hScreenDC);
                        pict = CreateCompatibleBitmap(hScreenDC, Size_.width, Size_.height);
                        SetBitmapBits(pict, data_all.size(), data_all.data());
                        BitBlt(hdc, 0, 0, Size_.width, Size_.height, hScreenDC, 0, 0, SRCCOPY);
                        SendMessage(hWnd, WM_PAINT, 0, 0);
                        InvalidateRect(hWnd, nullptr, true);

                        data_all.clear();

                        Sleep(1000);
                        write_s();
		}
                else
                {
                    deactivate();
                }
	}

        HBITMAP get_pict()
	{
            if (fl == WAMStatus::WAMStop || !active)
            {
                return nullptr;
            }
            return pict;
	}
private:
	struct size_
	{
		int width = 0;
		int height = 0;
	};

        MsgType mtype;
	size_ Size_;
	ssl_socket socket_;
	std::vector<char> data = std::vector<char>(60000);
	std::vector<char> data_all;
	HBITMAP pict;
	WAMStatus fl;
	const HWND& hWnd;
        bool active;
};

class server
{
public:
	server(const HWND& hw, boost::asio::io_service& io_service, unsigned short port)
	    : acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
            hWnd(hw), io_service_(io_service),
	    context_(boost::asio::ssl::context::sslv23),
            current_(0)
	{
		context_.set_options(boost::asio::ssl::context::sslv23);
                context_.use_certificate_chain(boost::asio::buffer(cert.data(), cert.size()));
                context_.use_private_key(boost::asio::buffer(kPrivateKey.data(), kPrivateKey.size()), boost::asio::ssl::context_base::pem);
                context_.use_tmp_dh(boost::asio::buffer(dh.data(), dh.size()));

                start_accept();
	}

	void start_accept()
	{
		std::shared_ptr<session> new_session(new session(hWnd, io_service_, context_));
                current_ = 0;
		acceptor_.async_accept(
                    new_session->socket(),
		    boost::bind(&server::handle_accept, this, new_session, boost::asio::placeholders::error)
                );
	}

	void handle_accept(std::shared_ptr<session> new_session,
		const boost::system::error_code& error)
	{
		if (!error)
		{
                        sessions_.push_back(new_session);
			new_session->start();
                        //start_current();
		}
		start_accept();
	}

	void stop()
	{
		acceptor_.cancel();
	}

        void start_current()
	{
            if (sessions_.empty())
            {
                return;
            }
            sessions_[current_]->enable();
	}

        void stop_current()
	{
            if (sessions_.empty())
            {
                return;
            }
            sessions_[current_]->disable();
	}

        void next()
	{
            if (sessions_.empty())
            {
                return;
            }
            stop_current();
            current_++;
            if (current_ >= sessions_.size())
            {
                current_ = 0;
            }
            start_current();
	}

        void prev()
	{
            if (sessions_.empty())
            {
                return;
            }
            stop_current();
            current_--;
            if (current_ < 0)
            {
                current_ = sessions_.size() - 1;
            }
            stop_current();
	}

        HBITMAP get_current_pict()
	{
            if (sessions_.empty())
            {
                return nullptr;
            }
            return sessions_[current_]->get_pict();
	}
    private:
        std::vector<std::shared_ptr<session>> sessions_;
        int current_;
	tcp::acceptor acceptor_;
	const HWND& hWnd;
	boost::asio::io_service& io_service_;
	boost::asio::ssl::context context_;
};

server* g_server = nullptr;

int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine, _In_ int nCmdShow)
{

	HWND hwnd;
	WNDCLASS WndClass;
	MSG Msg;
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = reinterpret_cast<WNDPROC>(PictViewWndProc);;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = L"szClassName";

	if (!RegisterClass(&WndClass)) {
		MessageBox(NULL, L"Can't register class", L"Error", MB_OK);
		return 0;
	}

	hwnd = CreateWindow(L"szClassName", L"Picture View", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

        boost::asio::io_service service;
        server s(hwnd, service, 8000);
        g_server = &s;

	if (!hwnd) {
		MessageBox(nullptr, L"Can't create window", L"Error", MB_OK);
		return 0;
	}

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
	std::thread service_loop([&service]()
            {
                service.run();
            });
	while (GetMessage(&Msg, nullptr, 0, 0))
        {
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	s.stop();
	service.stop();
	if (service_loop.joinable())
	{
		service_loop.join();
	}
	return Msg.wParam;
}

LRESULT CALLBACK PictViewWndProc(HWND hWnd, UINT Message, UINT wParam, LONG lParam)
{
	RECT Rect;
	static HWND startButton, stopButton, nextButton, previousButton;
	RECT rcClient, rcWind;
	GetClientRect(hWnd, &rcClient);
	GetWindowRect(hWnd, &rcWind);
	switch (Message) {
	case WM_CREATE:
	{
		startButton = CreateWindow(
			L"BUTTON",
			L"Start",
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			0,
			0,
			68,
			25,
			hWnd,
			NULL,
			(HINSTANCE)GetWindowLong(hWnd, GWLP_HINSTANCE),
			NULL);
		ShowWindow(startButton, SW_SHOWNORMAL);
		stopButton = CreateWindow(
			L"BUTTON",
			L"End",
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			0,
			30,
			68,
			25,
			hWnd,
			NULL,
			(HINSTANCE)GetWindowLong(hWnd, GWLP_HINSTANCE),
			NULL);
		ShowWindow(stopButton, SW_SHOWNORMAL);
		nextButton = CreateWindow(
			L"BUTTON",
			L"Next",
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			0,
			140,
			68,
			25,
			hWnd,
			NULL,
			(HINSTANCE)GetWindowLong(hWnd, GWLP_HINSTANCE),
			NULL);
		ShowWindow(nextButton, SW_SHOWNORMAL);
		previousButton = CreateWindow(
			L"BUTTON",
			L"Previous",
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			0,
			170,
			68,
			25,
			hWnd,
			NULL,
			(HINSTANCE)GetWindowLong(hWnd, GWLP_HINSTANCE),
			NULL);
		ShowWindow(previousButton, SW_SHOWNORMAL);
		break;
	}
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;
	case WM_COMMAND:
	{
		if (lParam == (LPARAM)startButton)
		{
                        g_server->start_current();
		}
		if (lParam == (LPARAM)stopButton)
		{
                        g_server->stop_current();
		}
		if (lParam == (LPARAM)nextButton)
		{
                        g_server->next();
		}
		if (lParam == (LPARAM)previousButton)
		{
                        g_server->prev();
		}
		break;
	}
	case WM_PAINT:
        {
                HDC hDC, hCompatibleDC;
                PAINTSTRUCT PaintStruct;
                BITMAP Bitmap;
                hDC = BeginPaint(hWnd, &PaintStruct);
                auto pict = g_server->get_current_pict();
                if (!pict)
                {
                    return 0;
                }
                GetObject(pict, sizeof(BITMAP), &Bitmap);
                hCompatibleDC = CreateCompatibleDC(hDC);
                GetClientRect(hWnd, &Rect);
                SelectObject(hCompatibleDC, pict);
                StretchBlt(hDC, 70, 0, Rect.right, Rect.bottom, hCompatibleDC, 0, 0, Bitmap.bmWidth,
                    Bitmap.bmHeight, SRCCOPY);
                DeleteDC(hCompatibleDC);
                return 0;
        }
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, Message, wParam, lParam);
}
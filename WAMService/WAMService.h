#pragma once

#include "ServiceBase.h"
#include "SystemProcess.h"
#include "ThreadPool.h"
#include <WtsApi32.h>
#include <ShlObj.h>

class WAMService : public CServiceBase
{
public:

	WAMService(PWSTR pszServiceName,
		BOOL fCanStop = TRUE,
		BOOL fCanShutdown = TRUE,
		BOOL fCanPauseContinue = FALSE);
	virtual ~WAMService(void);
	//virtual void OnStart(DWORD dwArgc, PWSTR* pszArgv);

protected:

	virtual void OnStart(DWORD dwArgc, PWSTR* pszArgv);
	virtual void OnStop();

	void ServiceWorkerThread(void);

private:
	
	//variables for service work
	BOOL m_fStopping;
	HANDLE m_hStoppedEvent;
	SystemProcess ScreenshotMaker;
};

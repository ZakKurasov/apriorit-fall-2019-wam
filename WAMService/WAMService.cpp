#pragma region Includes
#include "WAMService.h"

#pragma endregion


WAMService::WAMService(PWSTR pszServiceName,
	BOOL fCanStop,
	BOOL fCanShutdown,
	BOOL fCanPauseContinue)
	: CServiceBase(pszServiceName, fCanStop, fCanShutdown, fCanPauseContinue)
{
	m_fStopping = FALSE;

	// Create a manual-reset event that is not signaled at first to indicate 
	// the stopped signal of the service.
	m_hStoppedEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (m_hStoppedEvent == NULL)
	{
		throw GetLastError();
	}
}


WAMService::~WAMService(void)
{
	if (m_hStoppedEvent)
	{
		CloseHandle(m_hStoppedEvent);
		m_hStoppedEvent = NULL;
	}
}


void WAMService::OnStart(DWORD dwArgc, PWSTR* pszArgv)
{
	std::string s(ScreenshotMaker.GetPathFromRegistry() + "\\Components");
	std::wstring ws(s.begin(), s.end());
	ScreenshotMaker.Run(ws.c_str(), L"\\ScreenshotMaker.exe");
	//ScreenshotMaker.Run(L"C:\\Users\\pgmv2\\AppData\\Local\\WAMClient\\Components", L"\\ScreenshotMaker.exe");

	// Queue the main service function for execution in a worker thread.
	CThreadPool::QueueUserWorkItem(&WAMService::ServiceWorkerThread, this);
}


void WAMService::ServiceWorkerThread(void)
{
	// Periodically check if the service is stopping.
	while (!m_fStopping)
	{
		ScreenshotMaker.KeepAliveProcess();
	}

	// Signal the stopped event.
	SetEvent(m_hStoppedEvent);
}



void WAMService::OnStop()
{
	// Indicate that the service is stopping and wait for the finish of the 
	// main service function (ServiceWorkerThread).
	m_fStopping = TRUE;

	ScreenshotMaker.Stop();

	if (WaitForSingleObject(m_hStoppedEvent, INFINITE) != WAIT_OBJECT_0)
	{
		throw GetLastError();
	}
}
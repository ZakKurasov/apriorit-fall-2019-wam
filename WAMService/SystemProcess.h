#pragma once

#include <Windows.h>
#include <WtsApi32.h>
#include <string>

class SystemProcess
{

public:

	SystemProcess();

	~SystemProcess();


	BOOL Run(const wchar_t* path_, const wchar_t* file);

	void Stop();

	std::string GetPathFromRegistry();

	BOOL SetPrivilege(
		HANDLE hToken,          // access token handle
		LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
		BOOL bEnablePrivilege   // to enable or disable privilege
	);

	void KeepAliveProcess();

private:
	HANDLE processToken;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	HANDLE userToken;
	DWORD sessionId;
	char registryDirectory[256];
	std::wstring path;
	std::wstring file;
	bool stopped;
};

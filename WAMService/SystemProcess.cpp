#include "SystemProcess.h"

SystemProcess::SystemProcess()
	:processToken(INVALID_HANDLE_VALUE), si({}), pi({}), userToken(INVALID_HANDLE_VALUE), sessionId(NULL), stopped(false)
{
	sessionId= WTSGetActiveConsoleSessionId();
}

SystemProcess::~SystemProcess()	{}


BOOL SystemProcess::Run(const wchar_t* path_,const wchar_t* file_)
{
	stopped = false;
	path = path_;
	file = file_;
	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &processToken);
	if (!SetPrivilege(processToken, SE_ASSIGNPRIMARYTOKEN_NAME, TRUE))
	{
		return FALSE;
	}

	si.cb = sizeof(si);

	if (!WTSQueryUserToken(sessionId, &userToken))
	{
		return FALSE;
	}
	CreateProcessAsUserW(userToken, (path+file).c_str(), NULL, NULL, NULL, FALSE, 0, NULL, path.c_str(), &si, &pi);
	return TRUE;
}


void SystemProcess::Stop()
{
	stopped = true;
	TerminateProcess(pi.hProcess, 0);
	CloseHandle(userToken);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}

std::string SystemProcess::GetPathFromRegistry()
{
	HKEY rKey;
	DWORD RegetPath = sizeof(registryDirectory);
	RegOpenKeyExA(HKEY_LOCAL_MACHINE, "SOFTWARE\\WAMClient", NULL, KEY_QUERY_VALUE, &rKey);
	RegQueryValueExA(rKey, "Directory", NULL, NULL, (LPBYTE)&registryDirectory, &RegetPath);
	return std::string(registryDirectory);
}

void SystemProcess::KeepAliveProcess()
{
	WaitForSingleObject(pi.hProcess, INFINITE);
	if (!stopped)
	{
		CloseHandle(userToken);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
		Run(path.c_str(),file.c_str());
	}
}

BOOL SystemProcess::SetPrivilege(
	HANDLE hToken,          // access token handle
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;

	if (!LookupPrivilegeValue(
		NULL,            // lookup privilege on local system
		lpszPrivilege,   // privilege to lookup 
		&luid))        // receives LUID of privilege
	{
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (bEnablePrivilege)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	// Enable the privilege or disable all privileges.

	if (!AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		(PTOKEN_PRIVILEGES)NULL,
		(PDWORD)NULL))
	{
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

	{
		return FALSE;
	}

	return TRUE;
}